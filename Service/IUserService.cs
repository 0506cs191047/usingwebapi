﻿using System.Collections.Generic;
using UserAppWebAPICore3.Models;

namespace UserAppWebAPICore3.Service
{
    public interface IUserService
    {
        bool BlockUnblockUser(int id, bool blockStatus);
        bool DeleteUser(User user);
        bool DeleteUserById(int id);
        User Details(int id);
        User DetailsByName(string name);
        List<User> GetAllUsers();
        User Login(UserLoginInfo userLoginInfo);
        bool RegisterUser(User user);
        bool UpdateUser(User user);
    }
}

﻿namespace UserAppWebAPICore3.Service
{
    public interface ITokenGenerationService
    {
        string GenerateToken(int id,string name);
    }
}

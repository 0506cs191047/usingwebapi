﻿using System.Collections.Generic;
using UserAppWebAPICore3.Models;
using UserAppWebAPICore3.Repository;

namespace UserAppWebAPICore3.Service
{
    public class UserService : IUserService
    {
        readonly IUserRepository userRepository;
        public UserService(IUserRepository _userRepository) 
        { 
            userRepository = _userRepository;
        }

        public bool BlockUnblockUser(int id, bool blockStatus)
        {
            return userRepository.BlockUnblockUser(id, blockStatus);
        }

        public bool DeleteUser(User user)
        {
            User exists = userRepository.GetUserById(user.Id);
            if (exists == null)
            {
                return false;
            }
            else 
            {
                return userRepository.DeleteUser(exists);
            }
        }

        public bool DeleteUserById(int id)
        {
            User user = userRepository.GetUserById(id);
            return true;
        }

        public User Details(int id)
        {
            User user = userRepository.GetUserById(id);
            return user;
        }

        public User DetailsByName(string name)
        {
            return userRepository.GetUserByName(name);
        }

        public List<User> GetAllUsers()
        {
            return userRepository.GetAllUsers();
        }

        public User Login(UserLoginInfo userLoginInfo)
        {
            User isUserLoginDetailsExists = userRepository.Login(userLoginInfo);
            if (isUserLoginDetailsExists != null)
            {
                return isUserLoginDetailsExists;
            }
            else
            {
                return null;
            }
        }

        public bool RegisterUser(User user)
        {
            User exists = userRepository.GetUserByName(user.Name);
            if (exists == null)
            {
                return userRepository.RegisterUser(user);
            }
            else
            {
                return false;
            }
        }

        public bool UpdateUser(User user)
        {
            return userRepository.UpdateUser(user);
        }
    }
}

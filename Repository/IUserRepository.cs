﻿using System.Collections.Generic;
using UserAppWebAPICore3.Models;

namespace UserAppWebAPICore3.Repository
{
    public interface IUserRepository
    {
        bool BlockUnblockUser(int id, bool blockStatus);
        bool DeleteUser(User user);
        List<User> GetAllUsers();
        User GetUserById(int id);
        User GetUserById1(int id);
        User GetUserByName(string name);
        User Login(UserLoginInfo userLoginInfo);
        bool RegisterUser(User user);
        bool UpdateUser(User user);
    }
}

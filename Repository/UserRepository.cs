﻿using System.Collections.Generic;
using System.Linq;
using UserAppWebAPICore3.Context;
using UserAppWebAPICore3.Models;

namespace UserAppWebAPICore3.Repository
{
    public class UserRepository : IUserRepository
    {
        readonly UserDbContext userDbContext;
        public UserRepository(UserDbContext _userDbContext)
        {
            userDbContext = _userDbContext;
        }

        public bool BlockUnblockUser(int id, bool blockStatus)
        {
            User user =userDbContext.Users1.Where(u => u.Id == id).FirstOrDefault();
            if (user != null)
            {
                user.IsUserBlock = blockStatus;
                userDbContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteUser(User user)
        {
            userDbContext.Users1.Remove(user);
            userDbContext.SaveChanges();
            return true;
        }

        public List<User> GetAllUsers()
        {
            return userDbContext.Users1.Where(u => u.IsUserBlock == false).ToList();
        }

        public User GetUserById(int id)
        {
            return userDbContext.Users1.Where(u => u.Id == id).FirstOrDefault();
        }

        public User GetUserById1(int id)
        {
            User user = userDbContext.Users1.Where(u => u.Id == id).FirstOrDefault();
            userDbContext.Users1.Remove(user);
            userDbContext.SaveChanges();
            return user;

        }

        public User GetUserByName(string name)
        {
            return userDbContext.Users1.Where(u => u.Name == name).FirstOrDefault();
        }

        public User Login(UserLoginInfo userLoginInfo)
        {
           return userDbContext.Users1.Where(u => u.Name == userLoginInfo.Name && u.Password== userLoginInfo.Password).FirstOrDefault();
        }

        public bool RegisterUser(User user)
        {
            userDbContext.Users1.Add(user);
            userDbContext.SaveChanges();
            return true;
        }

        public bool UpdateUser(User user)
        {
            User users = userDbContext.Users1.Where(u => u.Name == user.Name).FirstOrDefault();
            users.Name = user.Name;
            users.Password = user.Password;
            users.City = user.City;
            users.Country = user.Country;
            userDbContext.SaveChanges();
            return true;
        }
    }
}

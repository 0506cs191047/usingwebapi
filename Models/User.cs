﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace UserAppWebAPICore3.Models
{
    public class User
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)] // In the place of Identity option we don't written the code of USerRepository for increment the Id it automatically generated.
        #region Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool IsRegular { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public bool IsUserBlock { get; set; } = false;
        #endregion
    }
}

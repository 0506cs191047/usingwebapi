﻿using Microsoft.EntityFrameworkCore;
using UserAppWebAPICore3.Models;

namespace UserAppWebAPICore3.Context
{
    public class UserDbContext : DbContext 
    {
        public UserDbContext(DbContextOptions<UserDbContext>options):base(options)
        {
            // Database.EnsureCreated();
        }
        // Table Creation
        public DbSet<User> Users1 { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UserAppWebAPICore3.Migrations
{
    public partial class IsUserBlockpropertyadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsUserBlock",
                table: "Users1",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsUserBlock",
                table: "Users1");
        }
    }
}

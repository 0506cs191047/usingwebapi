﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore.Update;
using System;
using UserAppWebAPICore3.Models;
using UserAppWebAPICore3.Service;

namespace UserAppWebAPICore3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IUserService userService;
        readonly ITokenGenerationService tokenGenerationService;
        public UserController(IUserService _userService, ITokenGenerationService _tokenGenerationService)
        {
            userService = _userService;
            tokenGenerationService = _tokenGenerationService;
        }
        #region GetAllUser
        [Route("getall")]
        [HttpGet]
        public ActionResult GetAllUsers()
        {
            return Ok(userService.GetAllUsers());
        }
        #endregion

        #region Details
        [Route("Details")]
        [HttpPost]
        public ActionResult Details(int id)
        {
            User user = userService.Details(id);
            if (user == null)
            {
                return Ok("User Details Not Found!!");
            }
            else
            {
                return Ok(user);
            }
        }
        [Route("UserDetailsByName/{name}")]
        [HttpPost]
        public ActionResult DetailsByName(string name)
        {
            User user = userService.DetailsByName(name);
            if (user == null)
            {
                return Ok("User Details Not Found!!");
            }
            else
            {
                return Ok(user);
            }

        }
        #endregion

        #region AddUser
        [Route("RegisterUser")]
        [HttpPost]
        public ActionResult RegisterUser(User user)
        {
            userService.RegisterUser(user);
            return Created("api/user/create",true);
        }
        #endregion

        #region Login
        [Route("Login")]
        [HttpPost]
        public ActionResult Login([FromBody] UserLoginInfo userLoginInfo)
        {
            // UserLoginInfo userLoginInfo = new UserLoginInfo();  // Also Create a that type of object.
            User user = userService.Login(userLoginInfo);
            if (user != null)
            {
                // Using Token -- Stay Login
                string tokenResult = tokenGenerationService.GenerateToken(user.Id,user.Name);
                return Ok(tokenResult);
            }
            else
            {
                return Ok("Login Failed");
            }
        }
        #endregion

        #region EditUser
        [Route("EditUser")]
        [HttpPost]
        public ActionResult EditUser(User user)
        {
            userService.UpdateUser(user);
            return EditUser(user);
        }
        #endregion

        #region DeleteUser
        [Route("DeleteUser")]
        [HttpPost]
        public ActionResult DeleteUser(User user)
        {
            userService.DeleteUser(user);
            return DeleteUser(user);
        }
        #endregion

        #region DeleteUserById
        [Route("DeleteUserById/{id}")]
        [HttpDelete]
        public ActionResult DeleteUserById(int id)
        {
           bool user = userService.DeleteUserById(id);
            return Ok(user);
        }
        #endregion

        #region BlockUnblockUser
        [Route("BlockUnblockUser/{id:int}/{blockStatus:bool}")]
        [HttpDelete]
        public ActionResult BlockUnblockUser(int id,bool blockStatus) 
        {
            bool blockunblockStatus =userService.BlockUnblockUser(id,blockStatus);
            return Ok(blockunblockStatus);
        }
        #endregion
    }
}